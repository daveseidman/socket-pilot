import { Fog, Quaternion, LoopOnce, BoxGeometry, MeshNormalMaterial, Clock, Scene, PerspectiveCamera, WebGLRenderer, Vector3, Euler, Object3D, Mesh, SphereBufferGeometry, TextureLoader, MeshBasicMaterial, AnimationMixer } from 'three';
import { BloomEffect, DepthOfFieldEffect, ChromaticAberrationEffect, EffectComposer, EffectPass, RenderPass } from 'postprocessing';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader';
import onChange from 'on-change';
import autoBind from 'auto-bind';
import { createEl, addEl } from 'lmnt';
import { degToRad, radToDeg } from './Utils';
import Socket from './Socket';

const draco = new DRACOLoader();
draco.setDecoderPath('./assets/draco/');

const accelThresh = 2.5;
// const SPEED = 10;
// const CAM_OFFSET = 5;
// const ROT_SPEED = 5;
const urlParams = new URLSearchParams(window.location.search);
const testingControls = urlParams.has('testcontrols');

export default class Airplane {
  constructor() {
    autoBind(this);

    const state = {
      connected: false,
      permission: false,
      steeringCentered: false, // TODO: implement this, user must hold rotation close to 0 for a second before starting
      gas: false,
      break: false,
    };

    window.degToRad = degToRad;
    this.rotationOffset = new Vector3();
    this.forwardSpeed = 0;
    this.maxSpeed = 5;
    this.heading = {
      yaw: 0,
      pitch: 0,
      roll: 0,
    };
    this.state = onChange(state, this.update);

    this.acceleration = { x: 0, y: 0, z: 0 };
    this.targetRotation = new Euler().set(degToRad(0), degToRad(90), -degToRad(-90), 'YXZ');
    this.rotation = new Euler();// .set(degToRad(0), degToRad(90), -degToRad(-90), 'YXZ');

    this.socket = new Socket('airplane');
    this.socket.on('roomCode', this.showRoomCode);
    this.socket.on('controlsFound', this.controlsFound);
    this.socket.on('orientationChange', ({ orientation }) => { this.orient = orientation; });
    this.socket.on('orientation', this.orientation);
    this.socket.on('motion', this.motion);
    this.socket.on('speed', this.speed);

    this.element = createEl('div', { className: 'airplane' });

    this.connect = createEl('div', { className: `airplane-connect ${testingControls ? 'hidden' : ''}` });
    this.connectCodeDescription = createEl('p', { className: 'airplane-connect-code-description', innerHTML: 'visit rx-co.de/pilot on your mobile device<br>and enter this code' });
    this.connectCode = createEl('div', { className: 'airplane-connect-code' });
    this.connectImageDescription = createEl('p', { className: 'airplane-connect-image-description', innerText: 'or scan the image below' });
    this.connectImage = createEl('img', { className: 'airplane-connect-image' });
    addEl(this.connect, this.connectCodeDescription, this.connectCode, this.connectImageDescription, this.connectImage);

    this.connected = createEl('div', { className: 'airplane-connected hidden', innerHTML: '<p>You are now connected<br>Click ENABLE GYROSCOPE to continue</p>' });

    this.setupScene().then(this.render);
    addEl(this.element, this.renderer.domElement, this.connect, this.connected);

    this.orient = 0;

    this.debug = createEl('div', { style: 'position: absolute; width: 100px; height: 25px; padding: 5px; color: white; background: black; font-size: 12px; font-family: courier-new; z-index: 10' });
    addEl(this.debug);

    this.clock = new Clock();
    this.update();
    window.addEventListener('resize', this.resize);
    window.addEventListener('keydown', this.keydown);
    window.addEventListener('keyup', this.keyup);
  }

  keydown({ key }) {
    if (key === 'ArrowLeft') {
      this.orientation({ alpha: 0, beta: -30, gamma: 0 });
      // setTimeout(() => { this.orientation({ alpha: 0, beta: 0, gamma: 0 }); }, 10);
    }
    if (key === 'ArrowRight') {
      this.orientation({ alpha: 0, beta: 30, gamma: 0 });
      // setTimeout(() => { this.orientation({ alpha: 0, beta: 0, gamma: 0 }); }, 10);
    }
    if (key === 'r') {
      this.planeOuter.position.set(0, 0, 0);
      this.action.play();
    }
  }

  keyup({ key }) {
    if (key === 'ArrowLeft' || key === 'ArrowRight') {
      this.orientation({ alpha: 0, beta: 0, gamma: 0 });
    }
  }

  setupScene() {
    return new Promise((resolve) => {
      this.scene = new Scene();
      this.mainCamera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 2000);
      this.camera = this.mainCamera;
      this.camera.position.set(0, 0, 50);
      this.renderer = new WebGLRenderer({ antialias: true });
      this.renderer.setSize(window.innerWidth, window.innerHeight);
      this.renderer.setPixelRatio(2);
      this.composer = new EffectComposer(this.renderer);
      this.composer.addPass(new RenderPass(this.scene, this.camera));
      this.dof = new DepthOfFieldEffect(this.camera, { focusDistance: 0.5, focalLength: 0.1, bokehScale: 1 });
      this.composer.addPass(new EffectPass(this.camera, this.dof));
      this.composer.addPass(new EffectPass(this.camera, new BloomEffect()));
      this.composer.addPass(new EffectPass(this.camera, new ChromaticAberrationEffect()));

      this.planeContainer = new Object3D();
      this.scene.add(this.planeContainer);

      this.planeOuter = new Object3D();
      this.planeOuter.rotation.x = degToRad(-90);
      this.planeOuter.position.y = 100;
      this.planeInner = new Object3D();
      this.planeInner.rotation.x = degToRad(90);
      this.planeOuter.add(this.planeInner);
      this.scene.add(this.planeOuter);

      this.orientContainer = new Object3D();
      // this.orientContainer.position.z = -5;
      this.motionContainer = new Object3D();
      this.orientContainer.add(this.motionContainer);
      this.planeInner.add(this.orientContainer);

      this.container = new Object3D();
      this.scene.add(this.container);

      const geometry = new SphereBufferGeometry(1000, 60, 40);
      // invert the geometry on the x-axis so that all of the faces point inward
      geometry.scale(-1, 1, 1);

      const texture = new TextureLoader().load('assets/images/envmap1.png');
      const material = new MeshBasicMaterial({ map: texture });

      this.background = new Mesh(geometry, material);

      this.scene.add(this.background);

      this.fog = new Fog(0xdceaef, 500, 1250);
      this.scene.fog = this.fog;

      const cityLoader = new GLTFLoader();
      cityLoader.setDRACOLoader(draco);
      cityLoader.load('./assets/models/city7.glb', (res) => {
        this.scene.add(res.scene);

        const airplaneLoader = new GLTFLoader();
        airplaneLoader.setDRACOLoader(draco);
        airplaneLoader.load('./assets/models/cockpit2.glb', (res) => {
          this.planeContainer.add(res.scene);
          this.plane = res.scene;

          this.yokeGeo = res.scene.getObjectByName('yoke');
          this.yokeGeo.rotation.order = 'YXZ';

          this.yoke = res.scene.getObjectByName('yoke_container');

          return resolve();
        });
      });

      if (testingControls) {
      // window.addEventListener('keydown', )
        this.state.permission = true;
      }

      this.angle = new Vector3(-1, 0, 0);
      window.Vector3 = Vector3;
    });
  }

  showRoomCode({ code }) {
    this.connectCode.innerText = code;
    this.connectImage.src = `https://api.qrserver.com/v1/create-qr-code/?size=600x600&data=http://rx-co.de/pilot/${code}`;
  }

  controlsFound({ os }) {
    this.controllerOS = os;
    console.log('connected to an', os);
    if (this.state.permission) return;
    this.connect.classList.add('hidden');
    this.connected.classList.remove('hidden');
    this.state.connected = true;
  }

  hideConnect() {
    this.connected.classList.add('hidden');
  }

  update(path, current, previous) {
    console.log(`${path}: ${previous} -> ${current}`);

    if (path === 'permission') {
      this.plane.add(this.camera);
      this.camera.position.set(0, 0, 0);

      if (current) {
        this.hideConnect();
      }
    }
  }

  orientation({ alpha, beta, gamma }) {
    this.rotation.set(degToRad(beta), degToRad(alpha) + degToRad(90), -degToRad(gamma), 'YXZ');
    this.yoke.quaternion.setFromEuler(this.rotation);

    const yaw = new Vector3(0, 0, -1).applyQuaternion(this.yoke.quaternion);
    const pitch = new Vector3(0, -1, 0).applyQuaternion(this.yoke.quaternion);
    const roll = new Vector3(-1, 0, 0).applyQuaternion(this.yoke.quaternion);
    this.heading.yaw = yaw.z;
    this.heading.pitch = pitch.y;
    this.heading.roll = roll.x;
  }

  motion({ x, y, z }) {
    if (!this.state.permission) {
      this.state.permission = true;
      this.hideConnect();
    }
    const vec = new Vector3(-x, y, z);
    this.acceleration.x += Math.abs(vec.x) >= accelThresh ? vec.x / 500 : 0;
    this.acceleration.y += Math.abs(vec.y) >= accelThresh ? vec.y / 500 : 0;
  }

  speed({ type, start }) {
    this.state[type] = start;
  }

  render() {
    this.composer.render(this.clock.getDelta());

    if (this.state.permission) {
      if (this.state.gas) {
        this.forwardSpeed += 0.01;
        this.forwardSpeed = Math.min(this.forwardSpeed, this.maxSpeed);
      } else {
        this.forwardSpeed *= 0.975;
      }
      this.planeContainer.rotateY(((this.heading.yaw / 4) * (this.forwardSpeed / 10)));
      this.planeContainer.rotateX((this.heading.pitch / 10) * (this.forwardSpeed / 10));
      this.planeContainer.rotateZ((this.heading.roll / 10) * (this.forwardSpeed / 10));
      this.planeContainer.translateZ(-this.forwardSpeed);
    }

    requestAnimationFrame(this.render);
  }

  resize() {
    this.mainCamera.aspect = window.innerWidth / window.innerHeight;
    this.mainCamera.updateProjectionMatrix();
    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }
}

// TODO: chop into four discrete input fields for better UI

import autoBind from 'auto-bind';
import onChange from 'on-change';
import { addEl, createEl } from 'lmnt';
import { Scene, PerspectiveCamera, WebGLRenderer, AmbientLight, Euler, Vector3, Quaternion } from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader';
import { getMobileOS, degToRad } from './Utils';
import Socket from './Socket';

const zee = new Vector3(0, 0, 1);
const DEV_PORT = '8080'; // TODO: get from env vars
const draco = new DRACOLoader();
draco.setDecoderPath('./assets/draco/');

export default class Controls {
  constructor() {
    autoBind(this);

    const state = {
      airplane: null,
      permission: false,
    };

    this.state = onChange(state, this.update);
    this.element = createEl('div', { className: 'controls' });
    this.debug = createEl('p', { className: 'controls-debug' });
    this.background = createEl('div', { className: 'controls-background' });
    this.enableButton = createEl('button', { className: 'controls-enable', innerText: 'Enable Gyroscope' }, {}, {
      click: () => {
        this.enableMotion();
        this.enableOrientation();
      },
    });

    this.code = createEl('div', { className: 'controls-code' });
    this.codeDescription = createEl('p', { className: 'controls-code-description', innerText: 'Enter your pilot code' });
    // TODO: maxlength not working
    this.codeInput = createEl('input', { className: 'controls-code-input', placeholder: 'CODE', type: 'text' }, { maxlength: 4, spellcheck: 'false' }, { keydown: this.checkForEnterPressed, blur: this.connect });
    this.codeHint = createEl('p', { className: 'controls-code-hint', innerHTML: 'No Code?<br>Visit daveseidman.com/pilot on your PC.' });
    addEl(this.code, this.codeDescription, this.codeInput, this.codeHint);
    addEl(this.element, this.background, this.code, this.enableButton);// ', this.debug);

    this.controller = createEl('div', { className: 'controls-controller hidden' });
    this.breakButton = createEl('button', { className: 'controls-controller-left', innerText: 'break' }, { type: 'break' }, { touchstart: this.speed, touchend: this.speed });
    this.gasButton = createEl('button', { className: 'controls-controller-right', innerText: 'gas' }, { type: 'gas' }, { touchstart: this.speed, touchend: this.speed });
    addEl(this.controller, this.breakButton, this.gasButton);

    addEl(this.element, this.controller);

    const { port, pathname } = window.location;
    if (port !== DEV_PORT) { // Not checking for code in URL on webpack-dev-server yet
      const codeInURL = pathname.split('/pilot/')[1].replace(/[^A-Za-z]/g, '');
      if (codeInURL.length >= 4) {
        this.codeInput.value = codeInURL.substring(0, 4);
        this.connect();
      }
    }
    document.body.addEventListener('contextmenu', (e) => { e.preventDefault(); });

    this.screenOrientation = window.orientation || 0;
    this.setupScene();
  }

  update(path, current, previous) {
    console.log(`${path}: ${previous} -> ${current}`);
    if (path === 'airplane') {
      this.code.classList[current ? 'add' : 'remove']('hidden');
    }

    if (path === 'permission') {
      this.enableButton.classList[current ? 'add' : 'remove']('hidden');
      this.controller.classList.remove('hidden');
    }
  }

  setupScene() {
    this.scene = new Scene();
    this.renderer = new WebGLRenderer({ antialias: true });
    this.camera = new PerspectiveCamera(25, 1, 0.1, 10);
    this.camera.position.z = 8;
    this.renderer.domElement.className = 'controls-scene';
    this.renderer.setSize(window.innerWidth, window.innerWidth);
    this.element.insertBefore(this.renderer.domElement, this.background);

    const loader = new GLTFLoader();
    loader.setDRACOLoader(draco);
    loader.load('assets/models/yoke.glb', (res) => {
      this.scene.add(res.scene);
      this.accelerometer = res.scene.getObjectByName('yoke');
      this.accelerometer.rotateX(degToRad(180));
      console.log(this.accelerometer);
      this.render();
    });
  }

  render() {
    this.renderer.render(this.scene, this.camera);
    requestAnimationFrame(this.render);
  }

  checkForEnterPressed({ key }) {
    if (key === 'Enter') this.connect();
    setTimeout(() => {
      if (this.codeInput.value.length === 4) this.connect();
    }, 100);
  }

  connect() {
    if (this.codeInput.value.length < 4) return;

    this.socket = new Socket('controls', this.codeInput.value, getMobileOS());

    this.socket.on('airplaneFound', ({ result }) => {
      if (result) {
        this.state.airplane = result.airplane;
        this.codeInput.removeEventListener('blur', this.connect);
        this.codeInput.blur();
        this.enableButton.focus();

        window.navigator.vibrate(500);
      } else {
        this.codeInput.classList.add('error');
      }
    });
    this.socket.on('airplaneLeft', () => {
      this.state.airplane = null;
    });
  }

  enableOrientation() {
    if (typeof DeviceOrientationEvent.requestPermission === 'function') {
      DeviceOrientationEvent.requestPermission().then((permissionState) => {
        if (permissionState === 'granted') {
          window.addEventListener('deviceorientation', this.deviceOrientation);
          window.addEventListener('orientationchange', this.orientationChange);
          this.state.permission = true;
        }
      }).catch(console.error);
    } else {
      window.addEventListener('deviceorientation', this.deviceOrientation);
      window.addEventListener('orientationchange', this.orientationChange);
      this.enableButton.classList.add('hidden');
      this.state.permission = true;
    }
  }

  enableMotion() {
    if (typeof DeviceMotionEvent.requestPermission === 'function') {
      DeviceMotionEvent.requestPermission().then((permissionState) => {
        if (permissionState === 'granted') {
          window.addEventListener('devicemotion', this.deviceMotion);
          this.enableButton.classList.add('hidden');
        }
      }).catch(console.error);
    } else {
      window.addEventListener('devicemotion', this.deviceMotion);
      this.enableButton.classList.add('hidden');
    }
  }

  orientationChange() {
    this.screenOrientation = window.orientation || 0;
    this.socket.emit('orientationChange', { airplane: this.state.airplane, orientation: window.orientation });
  }

  deviceOrientation({ alpha, beta, gamma }) {
    // this.debug.innerHTML = `${alpha.toFixed(2)}, ${beta.toFixed(2)}, ${gamma.toFixed(2)}`;
    if (this.accelerometer) {
      if (this.socket) this.socket.emit('orientation', { airplane: this.state.airplane, alpha, beta, gamma });
      const euler = new Euler();
      const q0 = new Quaternion();
      const q1 = new Quaternion(-Math.sqrt(0.5), 0, 0, Math.sqrt(0.5)); // - PI/2 around the x-axis
      const orient = this.screenOrientation ? degToRad(this.screenOrientation) : 0; //
      euler.set(-degToRad(beta), -degToRad(gamma), -degToRad(alpha), 'YXZ');
      this.accelerometer.quaternion.setFromEuler(euler); // orient the device
      this.accelerometer.quaternion.multiply(q1); // camera looks out the back of the device, not the top
      this.accelerometer.quaternion.multiply(q0.setFromAxisAngle(zee, -orient)); // adjust for screen orientation
    }
  }

  deviceMotion({ acceleration }) {
    const { x, y, z } = acceleration;
    this.socket.emit('motion', { airplane: this.state.airplane, x, y, z });
  }

  speed(e) {
    console.log(e.type, e.target.getAttribute('type'));
    // console.log('press', target.getAttribute('type'));
    this.socket.emit('speed', { airplane: this.state.airplane, type: e.target.getAttribute('type'), start: e.type === 'touchstart' });
  }
}

require('dotenv').config();

const express = require('express');
const fs = require('fs');
const https = require('https');
const http = require('http');
const chalk = require('chalk');

const app = express();

const port = parseInt(process.env.PORT, 10);
const rooms = {};

// remote server is behind actual SSL
// local uses express self signed cert
const server = process.env.SSL === 'true'
  ? http.createServer(app)
  : https.createServer({ key: fs.readFileSync('server/server.key'), cert: fs.readFileSync('server/server.cert') }, app);

const io = require('socket.io').listen(server);

server.listen(port, () => {
  console.log(`pilot server listening on ${port}`);
});

app.use('/pilot', express.static(`${__dirname}/../dist`)); // TODO: can probably use a wildcard here
app.use('/pilot/:code', express.static(`${__dirname}/../dist`));


const randomCode = () => [...Array(4)].map(() => String.fromCharCode(65 + Math.floor(Math.random() * 26))).join('');

const codeIsUnique = (code) => {
  let unique = true;
  for (let i = 0; i < Object.keys(rooms).length; i += 1) {
    if (code === rooms[Object.keys(rooms)[i]].code) unique = false;
    break;
  }
  return unique;
};


const createRoom = (socket) => {
  let code = randomCode();
  while (!codeIsUnique) code = randomCode();
  rooms[socket] = {
    code,
    airplane: socket,
    controls: null,
    controllerOS: null,
    status: 'waiting',
  };
  return code;
};

const getRoomByCode = (code) => {
  let matchingRoom = null;
  const keys = Object.keys(rooms);
  for (let i = 0; i < keys.length; i += 1) {
    if (rooms[keys[i]].code.toLowerCase() === code.toLowerCase()) {
      matchingRoom = rooms[keys[i]];
      break;
    }
  }
  return matchingRoom;
};

const getTypeBySocket = (socketID) => {
  // the default type is controls because if the airplane leaves before the controls do
  // the room is automatically deleted
  let type = 'controls';
  const keys = Object.keys(rooms);
  for (let i = 0; i < keys.length; i += 1) {
    if (socketID === rooms[keys[i]].controls) {
      type = 'controls';
      break;
    }
    if (socketID === rooms[keys[i]].airplane) {
      type = 'airplane';
      break;
    }
  }
  return type;
};

const getRoomBySocket = (socketID) => {
  let room = null;
  const keys = Object.keys(rooms);
  for (let i = 0; i < keys.length; i += 1) {
    if (socketID === rooms[keys[i]].airplane || socketID === rooms[keys[i]].controls) {
      room = rooms[keys[i]];
      break;
    }
  }
  return room;
};


io.on('connection', (socket) => {
  const { query } = socket.handshake;

  if (query.type === 'airplane') {
    const code = createRoom(socket.id);
    socket.emit('roomCode', { code });
    console.log(chalk.green('airplane connected:   ', code));
  }


  if (query.type === 'controls') {
    const { os, code } = query;
    const matchingRoom = getRoomByCode(code);
    if (matchingRoom) {
      console.log(chalk.green('controls connected:   ', code.toUpperCase()));
      matchingRoom.controls = socket.id;
      io.to(matchingRoom.airplane).emit('controlsFound', { os });
    }

    socket.emit('airplaneFound', { result: matchingRoom });

    socket.on('orientationChange', ({ airplane, orientation }) => {
      io.to(airplane).emit('orientationChange', { orientation });
    });

    socket.on('orientation', ({ airplane, alpha, beta, gamma }) => {
      io.to(airplane).emit('orientation', { alpha, beta, gamma });
    });

    socket.on('motion', ({ airplane, x, y, z }) => {
      io.to(airplane).emit('motion', { x, y, z });
    });

    socket.on('speed', ({ airplane, type, start }) => {
      io.to(airplane).emit('speed', { type, start });
    });
  }


  socket.on('disconnect', () => {
    const type = getTypeBySocket(socket.id);
    console.log(chalk.red(`${type} disconnected: ${getRoomBySocket(socket.id) ? getRoomBySocket(socket.id).code : ''}`));
    // TODO: send signals to controller or airplane if it's companion disconnected
    if (type === 'airplane') {
      console.log('controls socket from:', rooms[socket.id].controls);
      io.to(rooms[socket.id].controls).emit('airplaneLeft');
      delete (rooms[socket.id]);
    }
  });

  console.log('there are', Object.keys(rooms).length, 'rooms');
});
